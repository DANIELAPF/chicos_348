<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
    <h1>Lista de paises</h1>
    <table>
         <thead>
             <tr>
              <th>País</th>
              <th>Capital</th>
              <th>Moneda</th>
              <th>Población</th>
             </tr>
         </thead>
         <tbody>
         @foreach($paises as $pais=>$infopais)
         <tr>
         <td>
         {{ $pais}}
         </td>
         <td>
         {{ $infopais["capital"]}}
         </td>
         <td>
         {{ $infopais["moneda"]}}
         </td>
         <td>
         {{ $infopais["poblacion"]}}
         </td>
         </tr>
         @endforeach
         </tbody>
    </table>

</body>
</html>
