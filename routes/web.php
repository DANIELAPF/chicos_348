<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('paises', function () {
    //crear un arreglo con informacion de paises
    $paises = [
        "COLOMBIA" => [
            "capital" => "Bogota",
            "moneda" => "peso",
            "poblacion" => 50.372,
        ],
        "BOLIVIA" => [
            "capital" => "Sucre",
            "moneda" => "Bolivares",
            "poblacion" => 11.633,
        ],
        "BRASIL" => [
            "capital" => "Brazilia",
            "moneda" => "Dolar",
            "poblacion" => 212.216,
        ],
        "ECUADOR" => [
            "capital" => "Quito",
            "moneda" => "Dolar",
            "poblacion" => 17.517,
        ],
    ];
    //Recorrer la primera dimension de arreglo
   /* foreach ($paises as $pais => $infopais) {
        echo "<h2> $pais </h2>";
        echo "capital:" . $infopais["capital"]."<br/>";
        echo "Moneda:" . $infopais["moneda"]. "<br/>";
        echo "Poblacion(En millones de habitantes):" .$infopais["peso"]."<br/>";
        echo "<hr/>";
    }*/
    //mostrar una vista
    return view ('paises')->with("paises",$paises);

});
